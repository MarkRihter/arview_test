import moment, { Moment } from 'moment'

const DATE_FORMAT = 'YYYY-MM-DD'

export function fromStringToMoment(dateAsString: string): Moment {
	return moment(dateAsString)
}

export function fromMomentToString(date: Moment, format: string = DATE_FORMAT): string {
	return date.format(format)
}