import React, { FC, ChangeEvent, useState, useEffect } from 'react'
import moment from 'moment'
import { useParams, useHistory } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as yup from 'yup'

import { TimePicker } from '@material-ui/pickers'
import TextField from '@material-ui/core/TextField'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'

import BaseEvent, { EventType, isBaseEvent } from '../../models/BaseEvent'
import { saveEvent } from '../../store/actions'
import { ReducerState } from '../../store'

import './index.scss'

interface EventPageRouteParams { id?: string }

const EventPage: FC = () => {
	const { id } = useParams<EventPageRouteParams>()
	const history = useHistory()
	const dispatch = useDispatch()
	const {events} = useSelector((state: ReducerState) => state.app)
	const [eventType, setEventType] = useState<EventType>(3)
	const [isNewEvent, setIsNewEvent] = useState(true)
	const [isCancelDialogOpened, setCancelDialogOpenedState] = useState(false)
	const formik = useFormik({
		initialValues: {
			name: '',
			type: eventType,
			date: moment(new URL(location.href).searchParams.get('date')).hours(12).minutes(0),
			cost: '',
			note: '',
			address: '',
		},
		validationSchema: yup.object({
			name: yup
				.string()
				.min(4, 'Слишком коротко')
				.required('Введите название события'),
			type: yup
				.number()
				.required('Введите тип события'),
			cost: yup
				.string()
				.test('cost-required', 'Введите бюджет', value => !!(eventType !== EventType.HOLIDAY || value && value !== ''))
				.test('cost-is-number', 'Введите корректное значение бюджета', value => {
					const intValue = parseInt(value || '', 10)
					return eventType !== EventType.HOLIDAY || !isNaN(intValue) && intValue >= 0}
				),
			note: yup
				.string()
				.test('note-required', 'Введите заметку', value => !!(eventType !== EventType.ANOTHER || value && value !== ''))
				.test('note-length', 'Слишком коротко', value => !!(eventType !== EventType.ANOTHER || value && value.length > 3)),
			address: yup
				.string()
				.test('address-required', 'Введите адресс', value => !!(eventType !== EventType.ACTIVITY || value && value !== ''))
				.test('address-length', 'Слишком коротко', value => !!(eventType !== EventType.ACTIVITY || value && value.length > 3)),
		}),
		onSubmit: (values) => {
			let eventId: string
			if (typeof id === 'string') eventId = id
			else eventId = uuidv4()
			const event: BaseEvent = {...values, type: +values.type, id: eventId}

			dispatch(saveEvent(event))
		},
	})

	useEffect(() => {
		if (id) {
			const event = events.find((savedEvent: BaseEvent) => isBaseEvent(savedEvent) && savedEvent.id === id)
			if (typeof event !== 'undefined') {
				setEventType(+event.type)
				formik.setValues(event)
			}
			setIsNewEvent(!event)
		} else {
			setIsNewEvent(true)
		}
	}, [events])

	const resetAdditionalFields = () => {
		formik.setTouched(formik.initialTouched)
		formik.setErrors(formik.initialErrors)
		formik.setFieldValue('cost', '')
		formik.setFieldValue('date', formik.values.date.hours(12).minutes(0))
		formik.setFieldValue('address', '')
		formik.setFieldValue('note', '')
	}

	const renderTypeOptions = () => Object.keys(EventType).filter(type => !isNaN(parseInt(type, 10))).map(type => {
		let value
		switch(+type) {
		case EventType.ACTIVITY:
			value='Мероприятие'
			break
		case EventType.HOLIDAY:
			value='Праздник'
			break
		case EventType.ANOTHER:
			value='Другое'
			break
		}
		return <MenuItem key={type} value={type}>{value}</MenuItem>
	})

	return <div className='event-page-container'>
		<h1>{isNewEvent ? 'Добавить' : 'Изменить'} событие</h1>
		<Paper elevation={3} className='paper'>
			<TextField 
				id="name" 
				label="Название события"
				name="name"
				value={formik.values.name}
				onChange={formik.handleChange}
				error={formik.touched.name && Boolean(formik.errors.name)}
				helperText={formik.touched.name && formik.errors.name}
			/>
			<FormControl error={formik.touched.type && Boolean(formik.errors.type)}>
				<InputLabel id="type-label">Тип события</InputLabel>
				<Select
					labelId="type-label"
					id="type"
					name="type"
					value={formik.values.type}
					onChange={(e: ChangeEvent<any>) => {
						resetAdditionalFields()
						setEventType(parseInt(e.target.value, 10))
						formik.handleChange(e)
					}}
				>
					{renderTypeOptions()}
				</Select>
				{formik.touched.type && formik.errors.type && <FormHelperText>{formik.errors.type}</FormHelperText>}
			</FormControl>
			{eventType && eventType === EventType.ACTIVITY && 
				<>
					<TextField 
						id="address" 
						label="Адрес"
						name="address"
						value={formik.values.address}
						onChange={formik.handleChange}
						error={formik.touched.address && Boolean(formik.errors.address)}
						helperText={formik.touched.address && formik.errors.address}
					/>
					<TimePicker
						showTodayButton
						todayLabel="now"
						label="Время"
						ampm={false}
						value={formik.values.date}
						minutesStep={5}
						onChange={time => formik.setFieldValue('date', formik.values.date.hours(time?.hours() || 12).minutes(time?.minutes() || 0))}
					/>
				</>
			}
			{
				eventType && eventType === EventType.HOLIDAY &&
				<TextField 
					id="cost" 
					label="Бюджет"
					name="cost"
					value={formik.values.cost}
					onChange={formik.handleChange}
					error={formik.touched.cost && Boolean(formik.errors.cost)}
					helperText={formik.touched.cost && formik.errors.cost}
				/>
			}
			{
				eventType && eventType === EventType.ANOTHER && 
				<TextField 
					id="note" 
					label="Заметка"
					name="note"
					value={formik.values.note}
					onChange={formik.handleChange}
					error={formik.touched.note && Boolean(formik.errors.note)}
					helperText={formik.touched.note && formik.errors.note}
				/>
			}
		</Paper>
		<div className='button-container'>
			<Button 
				onClick={() => setCancelDialogOpenedState(true)} 
				variant="contained"
			>
				Отмена
			</Button>
			<Button
				onClick={() => formik.handleSubmit()}
				variant="contained" 
				color="primary"
			>
				Сохранить
			</Button>
		</div>

		<Dialog
			open={isCancelDialogOpened}
			onClose={() => setCancelDialogOpenedState(false)}
			aria-labelledby="alert-dialog-title"
			aria-describedby="alert-dialog-description"
		>
			<DialogTitle id="alert-dialog-title">Вы действительно хотите отменить изменения?</DialogTitle>
			<DialogActions>
				<Button onClick={() => setCancelDialogOpenedState(false)} color="primary">
					Нет
				</Button>
				<Button onClick={() => history.push('/')} color="primary" autoFocus>
					Да
				</Button>
			</DialogActions>
		</Dialog>
	</div>
}

export default EventPage