import React, { FC, useState, useEffect } from 'react'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import { DatePicker } from '@material-ui/pickers'
import { Badge } from '@material-ui/core'
import moment, { Moment } from 'moment'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

import EventCard from './components/EventCard'
import BaseEvent from '../../models/BaseEvent'
import { fromMomentToString } from '../../utils/momentUtils'
import { ReducerState } from '../../store'

import './index.scss'

const MainPage: FC = () => {
	const {events, eventDates} = useSelector((state: ReducerState) => state.app)
	const [selectedDate, setSelectedDate] = useState(moment())
	const [eventsForSelectedDate, setEventsForSelectedDate] = useState<BaseEvent[]>([])

	useEffect(() => {
		setEventsForSelectedDate(events.filter((savedEvent: BaseEvent) => savedEvent.date.isSame(selectedDate, 'day')))
	}, [selectedDate, events])

	const renderEvents = () => eventsForSelectedDate.map((event: BaseEvent) => <EventCard key={event.id} event={event}/>)
	const renderEventsEmpty = () => <Paper elevation={3} className='empty-event-list'><h1>На выбранную дату список событий пуст</h1></Paper>

	return <Container>
		<div
			className="main-page-container"
		>
			<div>
				<Paper elevation={3}>
					<DatePicker
						disableToolbar
						minDate={moment()}
						autoOk
						orientation="landscape"
						variant="static"
						openTo="date"
						value={selectedDate}
						onChange={date =>{ setSelectedDate(date as Moment)}}
						renderDay={(day, selectedDate, isInCurrentMonth, dayComponent) => {
							return eventDates.includes(fromMomentToString(moment(day))) ? <Badge color="secondary" badgeContent=" " variant="dot">
								{dayComponent}
							</Badge> : 
								dayComponent
						}}
					/>
				</Paper>
				<Link to={{pathname: '/event', search: `date=${fromMomentToString(selectedDate)}`}}>
					<Button variant="contained" color="primary" className="add-button">
					Добавить
					</Button>
				</Link>
			</div>
			<div className='event-list'>
				{eventsForSelectedDate.length === 0 ? renderEventsEmpty() : renderEvents()}
			</div>
		</div>
	</Container>
}

export default MainPage