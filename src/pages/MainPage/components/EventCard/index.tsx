import React, { FC, useState } from 'react'
import moment from 'moment'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import Editcon from '@material-ui/icons/Edit'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'

import BaseEvent from '../../../../models/BaseEvent'
import { isActivityEvent } from '../../../../models/ActivityEvent'
import { isHolidayEvent } from '../../../../models/HolidayEvent'
import { isAnotherEvent } from '../../../../models/AnotherEvent'
import { fromMomentToString } from '../../../../utils/momentUtils'
import { deleteEvent } from '../../../../store/actions'

import './index.scss'

interface EventCardProps {
    event: BaseEvent
}

const EventCard: FC<EventCardProps> = (props: EventCardProps) => {
	const [isDeleteModalOpened, setDeleteModalState] = useState(false)
	const dispatch = useDispatch()

	const onEventDelete = () => {
		dispatch(deleteEvent(props.event.id))
		setDeleteModalState(false)   
	}

	return <Paper elevation={3} className='event-card'>
		<div className='header'>
			<h3>{props.event.name}</h3>
			<div className='actions'>
				<Link to={{pathname: `/event/${props.event.id}`, search: `date=${fromMomentToString(props.event.date)}`}}>
					<IconButton>
						<Editcon fontSize="small" />
					</IconButton>
				</Link>
				<IconButton onClick={() => setDeleteModalState(true)}>
					<DeleteIcon fontSize="small" />
				</IconButton>
			</div>
		</div>
		<hr/>
		{isActivityEvent(props.event) && <p>Адрес: {props.event.address}</p>}
		{isActivityEvent(props.event) && <p>Время: {moment(props.event.date).format('HH:mm')}</p>}
		{isHolidayEvent(props.event) && <p>Бюджет: {props.event.cost}</p>}
		{isAnotherEvent(props.event) && <p>{props.event.note}</p>}

		<Dialog
			open={isDeleteModalOpened}
			onClose={() => setDeleteModalState(false)}
			aria-labelledby="alert-dialog-title"
			aria-describedby="alert-dialog-description"
		>
			<DialogTitle id="alert-dialog-title">Вы действительно хотите удалить событие?</DialogTitle>
			<DialogActions>
				<Button onClick={() => setDeleteModalState(false)} color="primary">
					Нет
				</Button>
				<Button onClick={onEventDelete} color="primary" autoFocus>
					Да
				</Button>
			</DialogActions>
		</Dialog>
	</Paper>
}

export default EventCard