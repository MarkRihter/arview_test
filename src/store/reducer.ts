
import { AnyAction } from 'redux'

import BaseEvent from '../models/BaseEvent'
import { UPDATE_EVENTS, UPDATE_EVENT_DATES } from './actionTypes'

export interface ReducerState {
    events: BaseEvent[];
    eventDates: string[];
}

const initialState: ReducerState = {
	events: [],
	eventDates: []
}

const reducer = (state = initialState, {type, payload}: AnyAction) => {
	switch (type) {
	case UPDATE_EVENTS:
		return { ...state, events: payload }
	case UPDATE_EVENT_DATES:
		return {...state, eventDates: payload }
	default:
		return state
	}   
}

export default reducer