import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'

import reducer from './reducer'
import sagas from './sagas'

export const history = createBrowserHistory()
const rootReducer = combineReducers({
	router: connectRouter(history),
	app: reducer
})
export type ReducerState = ReturnType<typeof rootReducer>
const sagaMiddleware = createSagaMiddleware()

const store = createStore(
	rootReducer,
	applyMiddleware(sagaMiddleware, routerMiddleware(history))
)

sagaMiddleware.run(sagas)

export default store