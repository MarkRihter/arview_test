import { all, put, takeLatest, takeEvery, select, call } from 'redux-saga/effects'
import { push } from 'connected-react-router'
import moment from 'moment'

import { DELETE_EVENT, GET_ALL_EVENTS, SAVE_EVENT, UPDATE_EVENTS } from './actionTypes'
import { updateEvents, updateEventDates, SaveEventAction, UpdateEventsAction, DeleteEventAction } from './actions'
import { ReducerState } from './index'

import BaseEvent, { isBaseEvent } from '../models/BaseEvent'
import { fromMomentToString } from '../utils/momentUtils'

function getEventDates(events: BaseEvent[]) {
	return events.reduce((dates: string[], event) => {
		const eventDateAsString = fromMomentToString(event.date)
		if (!dates.includes(eventDateAsString)) dates.push(eventDateAsString)
		return dates
	}, [])
}

function* getAllEventsWorker() {
	const stringifyedEvents = localStorage.getItem('events')
	if (typeof stringifyedEvents === 'string') {
		const events = JSON.parse(stringifyedEvents)
		if (Array.isArray(events)) {
			const eventsToSave = events.map(event => {
				event.date = moment(event.date)
				return event
			}).filter(event => isBaseEvent(event))
			yield put(updateEvents(eventsToSave))
		} else {
			localStorage.clear()
		}
	} else {
		localStorage.clear()
	}
}

function* saveEventWorker({payload}: SaveEventAction) {
	const events: BaseEvent[] = yield select((s => s.app.events))
	const existingEvent = events.find((savedEvent: BaseEvent) => savedEvent.id === payload.id)
	if (existingEvent) {
		const indexOfEvent = events.indexOf(existingEvent)
		events.splice(indexOfEvent, 1, payload)
	} else {
		events.push(payload)
	}
	localStorage.setItem('events', JSON.stringify(events))
	yield put(updateEvents(events))
	yield put(push('/'))
}

function* updateEventsWorker({payload}: UpdateEventsAction) {
	yield put(updateEventDates(getEventDates(payload)))
}

function* deleteEventsWorker({payload}: DeleteEventAction) {
	const events: BaseEvent[] = yield select((s => s.app.events))
	const newEvents = events.filter((event: BaseEvent) => event.id !== payload)
	localStorage.setItem('events', JSON.stringify(newEvents))
	yield put(updateEvents(newEvents))
}

export default function* contactsSaga() {
	yield all([
		takeLatest(GET_ALL_EVENTS, getAllEventsWorker),
		takeEvery(SAVE_EVENT, saveEventWorker),
		takeLatest(UPDATE_EVENTS, updateEventsWorker),
		takeEvery(DELETE_EVENT, deleteEventsWorker)
	])
}
  