import BaseEvent from '../models/BaseEvent'
import { GET_ALL_EVENTS, UPDATE_EVENTS, SAVE_EVENT, UPDATE_EVENT_DATES, DELETE_EVENT } from './actionTypes'

interface BaseAction {
	type: string;
}
export const getAllEvents = (): BaseAction => ({
	type: GET_ALL_EVENTS
})

export interface UpdateEventsAction extends BaseAction {
    payload: BaseEvent[]
}
export const updateEvents = (events: BaseEvent[]): UpdateEventsAction => ({
	type: UPDATE_EVENTS,
	payload: events
})

export interface SaveEventAction extends BaseAction {
    payload: BaseEvent;
}
export const saveEvent = (event: BaseEvent): SaveEventAction => ({
	type: SAVE_EVENT,
	payload: event
})

export interface UpdateEventDatesAction extends BaseAction {
    payload: string[]
}
export const updateEventDates = (eventDates: string[]): UpdateEventDatesAction => ({
	type: UPDATE_EVENT_DATES,
	payload: eventDates
})

export interface DeleteEventAction extends BaseAction {
	payload: string
}
export const deleteEvent = (eventId: string): DeleteEventAction => ({
	type: DELETE_EVENT,
	payload: eventId
})