import BaseEvent, {EventType, isBaseEvent} from './BaseEvent'

export function isAnotherEvent(obj: any): obj is AnotherEvent {
	return typeof obj.note === 'string' &&
        obj.type === EventType.ANOTHER &&
        isBaseEvent(obj)
}

export default interface AnotherEvent extends BaseEvent {
    type: EventType.ANOTHER;
    note: string
}