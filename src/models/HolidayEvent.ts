import BaseEvent, {EventType, isBaseEvent} from './BaseEvent'

export function isHolidayEvent(obj: any): obj is HolidayEvent {
	return typeof obj.cost === 'string' &&
        obj.type === EventType.HOLIDAY &&
        isBaseEvent(obj)
}

export default interface HolidayEvent extends BaseEvent {
    type: EventType.HOLIDAY;
    cost: string;
}