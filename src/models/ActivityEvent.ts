import BaseEvent, {EventType, isBaseEvent} from './BaseEvent'


export function isActivityEvent(obj: any): obj is ActivityEvent {
	return typeof obj.address === 'string' &&
        obj.type === EventType.ACTIVITY &&
        isBaseEvent(obj)
}

export default interface ActivityEvent extends BaseEvent {
    type: EventType.ACTIVITY;
    address: string;
}