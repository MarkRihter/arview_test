import moment, {Moment} from 'moment'

export function isBaseEvent(obj: any): obj is BaseEvent {
	return typeof obj.id === 'string' &&
    Object.values(EventType).includes(obj.type) &&
    typeof obj.name === 'string' &&
    moment.isMoment(obj.date)
}

export enum EventType {
    ANOTHER = 1,  
    HOLIDAY,
    ACTIVITY,  
}

export default interface BaseEvent {
    id: string;
    type: EventType;
    name: string;
    date: Moment;
}