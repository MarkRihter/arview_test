import React, { useEffect } from 'react'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import { ConnectedRouter } from 'connected-react-router'
import 'moment/locale/ru'
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import MainPage from './pages/MainPage'
import EventPage from './pages/EventPage'
import { history, ReducerState } from './store'
import { fromStringToMoment } from './utils/momentUtils'
import { getAllEvents } from './store/actions'

moment.locale('ru')

function App() {
	const dateString = new URL(location.href).searchParams.get('date')
	const selectedDate = dateString ? moment(fromStringToMoment(dateString)) : null
	const dispatch = useDispatch()
	const pathname = useSelector((state: ReducerState) => state.router.location.pathname)
	
	useEffect(() => {dispatch(getAllEvents())}, [])

	return (
		<MuiPickersUtilsProvider utils={MomentUtils} libInstance={moment} locale={'ru'}>
			<ConnectedRouter history={history}>
				<Switch>
					{pathname !== '/' && !selectedDate?.isValid() && <Redirect exact from="/event" to="/" />}
					<Route path="/event/:id">
						<EventPage />
					</Route>
					<Route path="/event">
						<EventPage />
					</Route>
					<Route exact path="/">
						<MainPage />
					</Route>
				</Switch>
			</ConnectedRouter>
		</MuiPickersUtilsProvider>
	)
}

export default App
